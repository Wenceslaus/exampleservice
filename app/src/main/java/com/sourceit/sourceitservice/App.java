package com.sourceit.sourceitservice;

import android.app.Application;
import android.content.Context;

/**
 * Created by wenceslaus on 24.06.17.
 */

public class App extends Application {

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }
}
